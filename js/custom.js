$( document ).ready(function() {

    $('#menu').multilevelpushmenu({
        collapsed: true,
        direction: 'rtl',
        backItemIcon: 'fa fa-angle-left',
        groupIcon: 'fa fa-angle-right',
        fullCollapse: true,  
        onExpandMenuStart: function() {
            $( '#menu' ).addClass("open");
        },
    });

    $( '#fullcollapse' ).click(function(){
        if ($(this).hasClass('close-menu')){
            $( '#menu' ).multilevelpushmenu( 'collapse' );
            $(this).removeClass("close-menu");
            $('.main-menu .language').removeClass('show-language');
        } else{
            $( '#menu' ).multilevelpushmenu( 'expand' );
            $(this).addClass("close-menu");
            $('.main-menu .language').addClass('show-language');
        }
    });                            

    
    $( '.levelHolderClass > h2 > .cursorPointer' ).click(function(){
        $( '#menu' ).removeClass("open");
    });

    

    var $filters = $('.filter [data-filter]'),
    $boxes = $('.boxes [data-filter]');

    $filters.on('click', function(e) {
        console.log('kk');
        e.preventDefault();
        var $this = $(this);

        $filters.removeClass('active');
        $this.addClass('active');

        var $filterColor = $this.attr('data-filter');

        if ($filterColor == 'all') {
            $boxes.removeClass('is-animated')
            .fadeOut().promise().done(function() {
                $boxes.addClass('is-animated').fadeIn();
            });
        } else {
            $boxes.removeClass('is-animated')
            .fadeOut().promise().done(function() {
                $boxes.filter('[data-filter = "' + $filterColor + '"]')
                .addClass('is-animated').fadeIn();
            });
        }
    });

    $(".gallery-cont").justifiedGallery({

        lastRow : 'nojustify',
        margins : 16,
        waitThumbnailsLoad: true,
        rowHeight : 112,

    });

    $(function(){
        $(".js-smartphoto, .lightbox").SmartPhoto();
    });

    var didScroll;
    var lastScrollTop = 0;
    var delta = 5;
    var navbarHeight = $('header').outerHeight();

    $(window).scroll(function(event){
        didScroll = true;
    });

    setInterval(function() {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 250);

    function hasScrolled() {
        var st = $(this).scrollTop();
        
        // Make sure they scroll more than delta
        if(Math.abs(lastScrollTop - st) <= delta)
            return;
        
        // If they scrolled down and are past the navbar, add class .nav-up.
        // This is necessary so you never see what is "behind" the navbar.
        if (st > lastScrollTop && st > navbarHeight){
            // Scroll Down
            $('header').removeClass('nav-down').addClass('nav-up');
        } else {
            // Scroll Up
            if(st + $(window).height() < $(document).height()) {
                $('header').removeClass('nav-up').addClass('nav-down');
            }
        }
        
        lastScrollTop = st;
    }

    function goToByScroll(section){

        var sliderH = $(section).parent().height();
        // Scroll
        $('html,body').animate({
            scrollTop: $(section).parent().offset().top+sliderH},
            'slow');
    }

    $(".arrow").click(function(e) { 
        // Prevent a page reload when a link is pressed
        e.preventDefault(); 
        // Call the scroll function
        goToByScroll(this);           
    });

    //Background move by mouse
    var lFollowX = 0,
    lFollowY = 0,
    x = 0,
    y = 0,
    friction = 1 / 30;

    function moveBackground() {
    x += (lFollowX - x) * friction;
    y += (lFollowY - y) * friction;
    
    translate = 'translate(' + x + 'px, ' + y + 'px) scale(1.1)';

    $('.cloud-bg').css({
        '-webit-transform': translate,
        '-moz-transform': translate,
        'transform': translate
    });

    window.requestAnimationFrame(moveBackground);
    }

    $(window).on('mousemove click', function(e) {

    var lMouseX = Math.max(-100, Math.min(100, $(window).width() / 2 - e.clientX));
    var lMouseY = Math.max(-100, Math.min(100, $(window).height() / 2 - e.clientY));
    lFollowX = (20 * lMouseX) / 100; // 100 : 12 = lMouxeX : lFollow
    lFollowY = (10 * lMouseY) / 100;

    });

    moveBackground();
});

